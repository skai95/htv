HTV mesh package

Contains
* 3D mesh of HTV7 with EDZ and sand lenses,
* 2D boundary meshes for
  * bottom,
  * mantle,
  * top.
